Introduction
============

The Premium Content Access Permissions By Term module allows access to content to be restricted to certain roles by taxonomy term and Drupal role permissions and supports multiple access tiers.

It does not use the node_access table.  It integrates with views via its use of taxonomy terms.  It is similar but different to the Taxonomy Access Control, TAC Lite, Restricted content and Premium modules.

When access is denied to a content item it supports redirecting the user to another path, or giving them the access denied page.  Support for more behaviors can be added reasonably easily.


Installation instructions
=========================

  1. Create a vocabulary for the access levels.  A suitable name for the vocabulary is "Restrict access to".  It must not be hierarchical or multiple.  It can be required if you want, though this is usually not recommended.
  2. Enable the vocabulary for at least one node type for which access can be restricted.
  3. Create one term for each premium level.  You don't need a term for "all users can access", since if a node has no access-level term set, then access control falls through to the "access content" permission.
  4. Add the following line to the end of settings.php and change 123 to the ID of the vocabulary you created;

        $conf['pcapbt_vocabulary_id'] = 123;

  5. If you want to redirect users to a page where they can promote themselves (e.g. by registering or purchasing a role), then add the following lines to the end of settings.php;

        $conf['pcapbt_actions'] = array(
          456 => array(
            'behavior' => 'redirect',
            'path' => 'user/login',
          ),
          789 => array(
            'behavior' => 'redirect',
            'path'  => 'node/1',
          ),
        );

  6. Change 456 and 789 to the term IDs for the access level terms you created.  Change 'user/login' and 'node/1' to the Drupal system paths that users should be redirected to when they need to upgrade their account.  If you don't configure any settings for a term, then it will simply return 403 access denied.  Currently only the 'redirect' behavior is supported, more behaviors can be added in the module, such as 'teaser', 'callback function', 'view', 'panel'.
  7. On the permissions page you can now set access permissions for each role and term you created.

Important Note
==============

If you change a term name you will need to reconfigure permissions.  This is a bug that needs to be fixed, by form_altering the permissions form so that the permission string uses the term ID, but the UI uses the term name.
